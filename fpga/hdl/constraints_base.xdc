set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_operating_conditions -ambient_temp 40.0

# 122.88 MHz
set_property -dict {PACKAGE_PIN P7} [get_ports clk122_p]
set_property -dict {PACKAGE_PIN P6} [get_ports clk122_n]
create_clock -period 8.1380208333 -name clk122 [get_ports clk122_p]
set_property -dict {PACKAGE_PIN E12 IOSTANDARD LVCMOS33 SLEW SLOW DRIVE 4} [get_ports clk122_en]

# Hyperram 1.8 V HPIO
set_property IOSTANDARD HSTL_I_18 [get_ports hram_*]
set_property SLEW FAST [get_ports hram_*]
set_property PACKAGE_PIN AD26 [get_ports hram_dq[0]]
set_property PACKAGE_PIN AC26 [get_ports hram_dq[1]]
set_property PACKAGE_PIN AF24 [get_ports hram_dq[2]]
set_property PACKAGE_PIN AF25 [get_ports hram_dq[3]]
set_property PACKAGE_PIN AD24 [get_ports hram_dq[4]]
set_property PACKAGE_PIN AE25 [get_ports hram_dq[5]]
set_property PACKAGE_PIN AA25 [get_ports hram_dq[6]]
set_property PACKAGE_PIN AC24 [get_ports hram_dq[7]]
set_property PACKAGE_PIN AB24 [get_ports hram_csn]
set_property PACKAGE_PIN AB26 [get_ports hram_clk]
set_property PACKAGE_PIN AE26 [get_ports hram_rwds]

# U.Fl debug connectors
set_property -dict {IOSTANDARD HSTL_I_18 SLEW FAST} [get_ports ufl[*]]
set_property PACKAGE_PIN AD16 [get_ports ufl[0]]
set_property PACKAGE_PIN AE16 [get_ports ufl[1]]

# LEDs
set_property -dict {IOSTANDARD LVCMOS33 SLEW SLOW DRIVE 4} [get_ports led[*]]
set_property PACKAGE_PIN A14 [get_ports led[0]]
set_property PACKAGE_PIN B14 [get_ports led[1]]

# PCI Express Gen4 x4
set_property LOC AC5 [get_ports "pcie_txp[0]"]
set_property LOC AC4 [get_ports "pcie_txn[0]"]
set_property LOC AD7 [get_ports "pcie_txp[1]"]
set_property LOC AD6 [get_ports "pcie_txn[1]"]
set_property LOC AF7 [get_ports "pcie_txp[2]"]
set_property LOC AF6 [get_ports "pcie_txn[2]"]
set_property LOC AE9 [get_ports "pcie_txp[3]"]
set_property LOC AE8 [get_ports "pcie_txn[3]"]
set_property LOC AB2 [get_ports "pcie_rxp[0]"]
set_property LOC AB1 [get_ports "pcie_rxn[0]"]
set_property LOC AD2 [get_ports "pcie_rxp[1]"]
set_property LOC AD1 [get_ports "pcie_rxn[1]"]
set_property LOC AF2 [get_ports "pcie_rxp[2]"]
set_property LOC AF1 [get_ports "pcie_rxn[2]"]
set_property LOC AE4 [get_ports "pcie_rxp[3]"]
set_property LOC AE3 [get_ports "pcie_rxn[3]"]
set_property -dict {LOC AC22 IOSTANDARD "LVCMOS18"} [get_ports "pcie_perst"]
set_property LOC AB7 [get_ports "pcie_refclk_p"]
set_property LOC AB6 [get_ports "pcie_refclk_n"]
create_clock -period 10 -name clk100_pci [get_ports pcie_refclk_p]

# QSFP control IO
set_property -dict {IOSTANDARD LVCMOS33 SLEW SLOW DRIVE 4} [get_ports qsfp_s*]
set_property -dict {IOSTANDARD LVCMOS33 SLEW SLOW DRIVE 4} [get_ports qsfp_en[*]]
set_property -dict {IOSTANDARD LVCMOS33 PULLUP TRUE} [get_ports qsfp_presentn[*]]
set_property LOC A12 [get_ports "qsfp_sda[0]"]
set_property LOC E13 [get_ports "qsfp_sda[1]"]
set_property LOC B12 [get_ports "qsfp_scl[0]"]
set_property LOC C12 [get_ports "qsfp_scl[1]"]
set_property LOC C13 [get_ports "qsfp_en[0]"]
set_property LOC A13 [get_ports "qsfp_en[1]"]
set_property LOC C14 [get_ports "qsfp_presentn[0]"]
set_property LOC F13 [get_ports "qsfp_presentn[1]"]

# QSFP GTH TX
#set_property LOC AA5 [get_ports "qsfp_txp[0]"]
#set_property LOC AA4 [get_ports "qsfp_txn[0]"]
#set_property LOC W5  [get_ports "qsfp_txp[1]"]
#set_property LOC W4  [get_ports "qsfp_txn[1]"]
#set_property LOC U5  [get_ports "qsfp_txp[2]"]
#set_property LOC U4  [get_ports "qsfp_txn[2]"]
#set_property LOC R5  [get_ports "qsfp_txp[3]"]
#set_property LOC R4  [get_ports "qsfp_txn[3]"]
#set_property LOC AC5 [get_ports "qsfp_txp[4]"]
#set_property LOC AC4 [get_ports "qsfp_txn[4]"]
#set_property LOC AD7 [get_ports "qsfp_txp[5]"]
#set_property LOC AD6 [get_ports "qsfp_txn[5]"]
#set_property LOC AF7 [get_ports "qsfp_txp[6]"]
#set_property LOC AF6 [get_ports "qsfp_txn[6]"]
#set_property LOC AE9 [get_ports "qsfp_txp[7]"]
#set_property LOC AE8 [get_ports "qsfp_txn[7]"]
# RX
#set_property LOC Y2  [get_ports "qsfp_rxp[0]"]
#set_property LOC Y1  [get_ports "qsfp_rxn[0]"]
#set_property LOC V2  [get_ports "qsfp_rxp[1]"]
#set_property LOC V1  [get_ports "qsfp_rxn[1]"]
#set_property LOC T2  [get_ports "qsfp_rxp[2]"]
#set_property LOC T1  [get_ports "qsfp_rxn[2]"]
#set_property LOC P2  [get_ports "qsfp_rxp[3]"]
#set_property LOC P1  [get_ports "qsfp_rxn[3]"]
#set_property LOC M2  [get_ports "qsfp_rxp[4]"]
#set_property LOC M1  [get_ports "qsfp_rxn[4]"]
#set_property LOC K2  [get_ports "qsfp_rxp[5]"]
#set_property LOC K1  [get_ports "qsfp_rxn[5]"]
#set_property LOC H2  [get_ports "qsfp_rxp[6]"]
#set_property LOC H1  [get_ports "qsfp_rxn[6]"]
#set_property LOC F2  [get_ports "qsfp_rxp[7]"]
#set_property LOC F1  [get_ports "qsfp_rxn[7]"]

# Current monitor I2C INA226 @ 0x44
set_property -dict {IOSTANDARD LVCMOS33 SLEW SLOW DRIVE 4} [get_ports imon_s*]
set_property LOC D13 [get_ports "imon_sda"]
set_property LOC D14 [get_ports "imon_scl"]

# Floorplanning
create_pblock pblock_x1y0
add_cells_to_pblock [get_pblocks pblock_x1y0] [get_cells -quiet [list pcie/pcie_core]]
resize_pblock [get_pblocks pblock_x1y0] -add {CLOCKREGION_X1Y0:CLOCKREGION_X1Y0}
