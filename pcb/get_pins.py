#!/usr/bin/env python3

from pcbnew import *

pcb = LoadBoard('ffvb676.kicad_pcb')
mod = pcb.FindFootprintByReference('U9')
pads = mod.Pads()

nets = {}

for pad in pads:
    name = pad.GetNetname()
    num = pad.GetNumber()
    print(name, num)
    nets[name] = num

print(nets)

def se(base, n, altname, dir='o'):
    pinlist = ""
    for i in range(n):
        ball = nets[base] if n == 1 else nets[f"{base}{i}"]
        pinlist += ball + ' '
    print(f"Subsignal(\"{altname}\", Pins(\"{pinlist[:-1]}\", dir='{dir}')),")

def dp(base, n, altname, dir='o'):
    pinlistp = ""
    pinlistn = ""
    for i in range(n):
        ball = nets[base+'+'] if n == 1 else nets[f"{base}{i}+"]
        pinlistp += ball + ' '
        ball = nets[base+'-'] if n == 1 else nets[f"{base}{i}-"]
        pinlistn += ball + ' '
    print(f"Subsignal(\"{altname}\", DiffPairs(\"{pinlistp[:-1]}\", \"{pinlistn[:-1]}\", dir='{dir}')),")

se("/LED", 2, "led")

for i in [0, 1]:
    print(f"{i} quad")
    se(f"/SCL{i}", 1, "scl")
    se(f"/SDA{i}", 1, "sda")
    se(f"/~{{SFPRST{i}}}", 1, "reset")

for i in range(16):
    print(f'Resource("mgt", {i},')
    print("    ", end='')
    dp(f"/MGT/TX{i}", 1, "tx", dir='o')
    print("    ", end='')
    dp(f"/MGT/RX{i}", 1, "rx", dir='i')
    print("),")
