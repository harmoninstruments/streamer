create_project streamer build_base -part xcau10p-ffvb676-2-e

create_ip -name pcie4c_uscale_plus -vendor xilinx.com -library ip -version 1.0 -module_name pcie_core
set_property -dict [list \
  CONFIG.AXISTEN_IF_RC_STRADDLE {false} \
  CONFIG.AXISTEN_IF_RQ_ALIGNMENT_MODE {Address_Aligned} \
  CONFIG.PL_LINK_CAP_MAX_LINK_SPEED {16.0_GT/s} \
  CONFIG.PL_LINK_CAP_MAX_LINK_WIDTH {X4} \
  CONFIG.axisten_if_enable_client_tag {true} \
  CONFIG.cfg_ctl_if {false} \
  CONFIG.cfg_fc_if {false} \
  CONFIG.cfg_mgmt_if {false} \
  CONFIG.cfg_pm_if {false} \
  CONFIG.cfg_status_if {false} \
  CONFIG.cfg_tx_msg_if {false} \
  CONFIG.en_ext_ch_gt_drp {true} \
  CONFIG.en_gt_selection {true} \
  CONFIG.en_pcie_conf {false} \
  CONFIG.gt_drp_clk_src {Internal} \
  CONFIG.disable_gt_loc {true} \
  CONFIG.mode_selection {Advanced} \
  CONFIG.pf0_bar0_64bit {true} \
  CONFIG.pf0_bar0_scale {Megabytes} \
  CONFIG.pf0_bar0_size {1} \
  CONFIG.pf0_base_class_menu {Data_acquisition_and_signal_processing_controllers} \
  CONFIG.pf0_dsn_enabled {false} \
  CONFIG.pf0_msi_enabled {false} \
  CONFIG.pf0_sub_class_interface_menu {Other_data_acquisition/signal_processing_controllers} \
  CONFIG.rcv_msg_if {false} \
  CONFIG.select_quad {GTH_Quad_224} \
  CONFIG.sys_reset_polarity {ACTIVE_HIGH} \
  CONFIG.tx_fc_if {true} \
] [get_ips pcie_core]

create_ip -name system_management_wiz -vendor xilinx.com -library ip -version 1.3 -module_name sysmon_wrap
set_property -dict [list \
  CONFIG.AVERAGE_ENABLE_VUSER1 {false} \
  CONFIG.AVERAGE_ENABLE_VUSER2 {false} \
  CONFIG.AVERAGE_ENABLE_VUSER3 {false} \
  CONFIG.CHANNEL_ENABLE_VBRAM {false} \
  CONFIG.CHANNEL_ENABLE_VP_VN {false} \
  CONFIG.CHANNEL_ENABLE_VUSER0 {true} \
  CONFIG.CHANNEL_ENABLE_VUSER1 {true} \
  CONFIG.CHANNEL_ENABLE_VUSER2 {true} \
  CONFIG.CHANNEL_ENABLE_VUSER3 {true} \
  CONFIG.DCLK_FREQUENCY {100} \
  CONFIG.SELECT_USER_SUPPLY0_LEVEL {1.2} \
  CONFIG.SELECT_USER_SUPPLY1 {AVTT} \
  CONFIG.SELECT_USER_SUPPLY1_LEVEL {1.2} \
  CONFIG.SELECT_USER_SUPPLY2 {MGTVCCAUX} \
  CONFIG.SELECT_USER_SUPPLY3_LEVEL {3.3} \
  CONFIG.SERIAL_INTERFACE {None} \
  CONFIG.USER_SUPPLY0_ALARM {true} \
  CONFIG.USER_SUPPLY0_ALARM_LOWER {0.85} \
  CONFIG.USER_SUPPLY0_ALARM_UPPER {0.95} \
  CONFIG.USER_SUPPLY0_BANK {224} \
  CONFIG.USER_SUPPLY1_ALARM {true} \
  CONFIG.USER_SUPPLY1_ALARM_LOWER {1.15} \
  CONFIG.USER_SUPPLY1_ALARM_UPPER {1.25} \
  CONFIG.USER_SUPPLY1_BANK {224} \
  CONFIG.USER_SUPPLY2_ALARM {true} \
  CONFIG.USER_SUPPLY2_ALARM_LOWER {1.75} \
  CONFIG.USER_SUPPLY2_ALARM_UPPER {1.85} \
  CONFIG.USER_SUPPLY2_BANK {224} \
  CONFIG.USER_SUPPLY3_ALARM {true} \
  CONFIG.USER_SUPPLY3_ALARM_LOWER {3.2} \
  CONFIG.USER_SUPPLY3_ALARM_UPPER {3.4} \
  CONFIG.USER_SUPPLY3_BANK {86} \
] [get_ips sysmon_wrap]

create_ip -name gtwizard_ultrascale -vendor xilinx.com -library ip -version 1.7 -module_name gtwizard_226
set_property -dict \
    [list \
         CONFIG.CHANNEL_ENABLE {X0Y11 X0Y10 X0Y9 X0Y8} \
         CONFIG.RX_DATA_DECODING {64B66B} \
         CONFIG.RX_LINE_RATE {16} \
         CONFIG.RX_MASTER_CHANNEL {X0Y8} \
         CONFIG.RX_QPLL_FRACN_NUMERATOR {3495253} \
         CONFIG.TX_DATA_ENCODING {64B66B} \
         CONFIG.TX_LINE_RATE {16} \
         CONFIG.TX_MASTER_CHANNEL {X0Y11} \
         CONFIG.TX_QPLL_FRACN_NUMERATOR {3495253} \
         CONFIG.RX_REFCLK_FREQUENCY {122.88} \
         CONFIG.RX_REFCLK_SOURCE {} \
         CONFIG.TX_REFCLK_FREQUENCY {122.88} \
         CONFIG.LOCATE_RX_USER_CLOCKING {EXAMPLE_DESIGN} \
         CONFIG.LOCATE_TX_USER_CLOCKING {EXAMPLE_DESIGN} \
         CONFIG.LOCATE_RESET_CONTROLLER {EXAMPLE_DESIGN} \
         CONFIG.LOCATE_USER_DATA_WIDTH_SIZING {EXAMPLE_DESIGN} \
         CONFIG.ENABLE_OPTIONAL_PORTS {drpaddr_common_in drpaddr_in drpclk_common_in \
                                           drpclk_in drpdi_common_in drpdi_in \
                                           drpen_common_in drpen_in drpwe_common_in \
                                           drpwe_in qpll0pd_in rxpd_in rxpolarity_in \
                                           sdm0data_in sdm0toggle_in sdm0width_in \
                                           txpd_in txpolarity_in drpdo_common_out \
                                           drpdo_out drprdy_common_out drprdy_out \
                                       } \
        ] [get_ips gtwizard_226]

set_msg_config -suppress -id {Synth 8-7080}

add_files -fileset constrs_1 -norecurse hdl/constraints_base.xdc
add_files -scan_for_includes [ glob -nocomplain hdl/*.sv ]
