module streamer_base
  (
   input        clk122_p, clk122_n,
   output       clk122_en,
   output       hram_clk, hram_csn, hram_rwds,
   inout [7:0]  hram_dq,
   output [1:0] ufl,
   output [1:0] led,
   output [3:0] pcie_txp, pcie_txn,
   input [3:0]  pcie_rxp, pcie_rxn,
   input        pcie_refclk_p, pcie_refclk_n,
   input        pcie_perst,
   inout [1:0]  qsfp_sda, qsfp_scl,
   output [1:0] qsfp_en,
   input [1:0]  qsfp_presentn,
   //output [7:0] qsfp_txp, qsfp_txn,
   //input [7:0]  qsfp_rxp, qsfp_rxn,
   inout        imon_sda, imon_scl
   );

   wire         clk250_pci, clk100_pci;

   pcie pcie
     (
      .clk250_pci,
      .clk100_pci,
      .rst250_pci(),
      // local bus
      // PCIe IO pins
      .pcie_txp, .pcie_txn,
      .pcie_rxp, .pcie_rxn,
      .pcie_perst,
      .pcie_refclk_p, .pcie_refclk_n
      );

   sysmon_wrap sysmon_wrap
     (
      .daddr_in(8'd0),
      .dclk_in(clk100_pci),
      .den_in(1'b0),
      .di_in(16'h0),
      .dwe_in(1'b0),
      .reset_in(1'b0),
      .vccaux_alarm_out(),
      .vccint_alarm_out(),
      .user_temp_alarm_out(),
      .busy_out(),
      .channel_out(),
      .do_out(),
      .drdy_out(),
      .eoc_out(),
      .eos_out(),
      .ot_out(),
      .user_supply0_alarm_out(),
      .user_supply1_alarm_out(),
      .user_supply2_alarm_out(),
      .user_supply3_alarm_out(),
      .alarm_out()
      );

   wire [31:0]  icap_o;
   wire         prdone, prerror, icap_avail;

   ICAPE3 #() ICAPE3_i(
      .AVAIL(icap_avail),
      .O(icap_o),
      .PRDONE(prdone),
      .PRERROR(prerror),
      .CLK(clk100_pci),
      .CSIB(1'b1),
      .I(32'h0),
      .RDWRB(1'b0)
   );

endmodule
