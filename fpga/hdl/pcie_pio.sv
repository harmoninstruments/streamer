typedef enum logic [1:0] {IDLE, FLUSH, READ, PUSH} states;

module pcie_pio
  (
   input             clock,
   input             reset,
   // local bus
   output bit [63:0] local_wdata,
   output bit [16:0] local_addr, // 19:3 of byte address
   output bit        local_wren,
   output bit        local_valid = 0,
   input             local_ready,
   input [63:0]      local_rdata,
   input             local_rvalid,
   // completer request AXI stream
   input [255:0]     m_axis_cq_tdata,
   input [87:0]      m_axis_cq_tuser, // using byte enables, sop, disc
   input             m_axis_cq_tlast,
   input             m_axis_cq_tvalid,
   output            m_axis_cq_tready,
   // completer completion AXI stream
   output [255:0]    s_axis_cc_tdata,
   output bit        s_axis_cc_tlast = 0,
   output bit [7:0]  s_axis_cc_tkeep = 0,
   output bit        s_axis_cc_tvalid = 0,
   input [3:0]       s_axis_cc_tready
   );

   assign s_axis_cc_tdata[6:0] = {m_axis_cq_tdata[6:2],2'b0}; // cq lower addr
   assign s_axis_cc_tdata[9:8] = m_axis_cq_tdata[1:0]; // address type

   assign s_axis_cc_tdata[71:64] = 1'b0; // cq tag
   assign s_axis_cc_tdata[72:87] = 1'b0; // completer bus/function filled by hard block
   assign s_axis_cc_tdata[88] = 1'b0; // completer ID enable
   assign s_axis_cc_tdata[91:89] = 1'b0; // TC from cq
   assign s_axis_cc_tdata[94:92] = 1'b0; // Attributes from cq
   assign s_axis_cc_tdata[95] = 1'b0; // Force ECRC

   wire [1:0]         address_type = m_axis_cq_tdata[1:0];
   wire [61:0]        address = m_axis_cq_tdata[63:2];
   wire [10:0]        dword_count = m_axis_cq_tdata[74:64];
   wire [3:0]         request_type = m_axis_cq_tdata[78:75];
   wire [15:0]        requester_id = m_axis_cq_tdata[95:80];
   wire [7:0]         tag = m_axis_cq_tdata[103:96];
   // 111:104 target function
   // 114:112 BAR ID
   // 120:115 BAR aperture
   wire [2:0]         transaction_class = m_axis_cq_tdata[123:121];
   wire [2:0]         attributes = m_axis_cq_tdata[126:124];

   wire [31:0]        byte_en = m_axis_cq_tuser[39:8];
   wire               sop = m_axis_cq_tuser[40];
   wire               discontinue = m_axis_cq_tuser[41]; // packet is all bad asserted with tlast

   states state = IDLE;

   assign m_axis_cq_tready = state == IDLE;

   // states
   // idle
   // read wait
   // push wait

   // write - no response
   // read -

   always_ff @ (posedge clock)
     begin
        if(reset)
          state <= IDLE;

     end

endmodule
