module pcie
  (
   output       clk250_pci,
   output       clk100_pci,
   output       rst250_pci,
   // IO pins
   output [3:0] pcie_txp, pcie_txn,
   input [3:0]  pcie_rxp, pcie_rxn,
   input        pcie_refclk_p, pcie_refclk_n,
   input        pcie_perst
   );

   // requester request AXI stream
   wire         s_axis_rq_tlast = 1;
   wire [255:0] s_axis_rq_tdata = 0;
   wire [61:0]  s_axis_rq_tuser = 0;
   wire [7:0]   s_axis_rq_tkeep = 8'hFF;
   wire [3:0]   s_axis_rq_tready;
   wire         s_axis_rq_tvalid = 0;

   // requester completion AXI stream, always ready, don't request anything you can't accept
   // this is only responses to DMA read requests
   wire [255:0] m_axis_rc_tdata;
   wire [74:0]  m_axis_rc_tuser; // only using 42 (disc), 32 (sof)
   wire         m_axis_rc_tlast;
   wire         m_axis_rc_tvalid;

   // completer request AXI stream
   wire [255:0] m_axis_cq_tdata;
   wire [87:0]  m_axis_cq_tuser;
   wire         m_axis_cq_tlast;
   wire         m_axis_cq_tvalid;
   wire         m_axis_cq_tready = 1;

   // completer completion AXI stream
   wire [255:0] s_axis_cc_tdata = 0;
   wire         s_axis_cc_tlast = 0;
   wire [7:0]   s_axis_cc_tkeep = 0;
   wire         s_axis_cc_tvalid = 0;
   wire [3:0]   s_axis_cc_tready;

   wire         pcie_refclk_gt, pci_refclk_ibuf;
   IBUFDS_GTE4 #(.REFCLK_HROW_CK_SEL(2'h0)) ibufds_pcie_refclk // HROW = 0: divide by 1 on ODIV2
     (
      .CEB(1'h0),
      .I(pcie_refclk_p),
      .IB(pcie_refclk_n),
      .O(pcie_refclk_gt),
      .ODIV2(pcie_refclk_ibuf)
      );

   pcie_core  pcie_core
     (
      // DRP
      .ext_ch_gt_drpclk(clk100_pci),
      .ext_ch_gt_drpaddr(40'b0),
      .ext_ch_gt_drpen(4'b0),
      .ext_ch_gt_drpdi(64'd0),
      .ext_ch_gt_drpwe(4'b0),
      .ext_ch_gt_drpdo(),
      .ext_ch_gt_drprdy(),
      // Pins
      .pci_exp_txp(pcie_txp), .pci_exp_txn(pcie_txn),
      .pci_exp_rxp(pcie_rxp), .pci_exp_rxn(pcie_rxn),
      // Clock, status outs
      .user_clk(clk250_pci),
      .user_reset(rst250_pci),
      .user_lnk_up(),
      .phy_rdy_out(),
      // AXI streams
      .s_axis_rq_tlast, .s_axis_rq_tdata, .s_axis_rq_tuser,
      .s_axis_rq_tkeep, .s_axis_rq_tready, .s_axis_rq_tvalid,

      .m_axis_rc_tdata, .m_axis_rc_tuser, .m_axis_rc_tlast,
      .m_axis_rc_tkeep(), .m_axis_rc_tvalid, .m_axis_rc_tready(1'b1),

      .m_axis_cq_tdata, .m_axis_cq_tuser, .m_axis_cq_tlast,
      .m_axis_cq_tkeep(), .m_axis_cq_tvalid, .m_axis_cq_tready,

      .s_axis_cc_tdata, .s_axis_cc_tuser(33'h0), .s_axis_cc_tlast,
      .s_axis_cc_tkeep, .s_axis_cc_tvalid, .s_axis_cc_tready,
      // Flow control
      .pcie_tfc_nph_av(), // 4 b each
      .pcie_tfc_npd_av(),
      // Interrupts
      .cfg_interrupt_int(4'd0),
      .cfg_interrupt_pending(4'd0),
      .cfg_interrupt_sent(),
      // clock and reset in
      .sys_clk(pcie_refclk_ibuf),
      .sys_clk_gt(pcie_refclk_gt),
      .sys_reset(pcie_perst)
);

endmodule
